<?php

namespace App;

interface CoefficientInterface
{
    public function calculate(int $arg1, int $arg2): int ;
}