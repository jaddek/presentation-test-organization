<?php

declare(strict_types=1);

namespace App;

use App\Exception\ArgumentException;
use App\Exception\EqualException;

class Coefficient implements CoefficientInterface
{
    /**
     * @param int $arg1
     * @param int $arg2
     * @return int
     * @throws ArgumentException
     * @throws EqualException
     */
    public function calculate(int $arg1, int $arg2): int
    {
        if ($arg1 > 10) {
            throw new ArgumentException('Argument 1 too low');
        }

        //@FIXME:!!!!!!!!!!!!!!!
        if ($arg2 === $arg1) {
            throw new EqualException('Arguments cant be equal');
        }

        return $arg2 - $arg1;
    }
}