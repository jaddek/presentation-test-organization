<?php

declare(strict_types=1);

namespace App;

use App\Exception\ArgumentException;

class Calculator
{
    private $coefficientStrategy;

    /**
     * FIXME: все ли хорошо в конструкторе?
     *
     * Calculator constructor.
     * @param Coefficient $strategy
     */
    public function __construct(Coefficient $strategy)
    {
        $this->coefficientStrategy = $strategy;
    }

    public function sum(int $arg1, int $arg2): int
    {
        if ($arg1 < 0) {
            throw new ArgumentException('Args cant be less than 0');
        }

        return $arg1 + $arg2;
    }

    public function squareOfSum(int $arg1, int $arg2): int
    {
        $sum = $this->sum($arg1, $arg2);

        return $sum * $sum;
    }

    public function sumPlusCoefficient($arg1,  $arg2)
    {
        return $arg1 + $arg2 + $this->coefficientStrategy->calculate($arg1, $arg2);
    }
}