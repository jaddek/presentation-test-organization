<?php

namespace App\Tests\Acceptance;

use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;

class ActionTest extends TestCase
{
    public function testGetCalculate()
    {
        $client = new Client(['base_uri' => 'http://localhost:8080/']);
        $response = $client->get(sprintf('calculate/%s/%s', 10, 200));
        $this->assertSame('210', $response->getBody()->getContents());

    }
}