<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Calculator;
use App\Coefficient;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{
    public function sumPlusCoefficientProvider()
    {
        return [
            [
                1,
                2,
                new Coefficient(),
            ],
        ];
    }

    /**
     * @dataProvider sumPlusCoefficientProvider
     *
     * @param $arg1
     * @param $arg2
     * @param $coefficient
     */
    public function testSumPlusCoefficient($arg1, $arg2, $coefficient)
    {
        $calculator = new Calculator($coefficient);
        $result     = $calculator->sumPlusCoefficient($arg1, $arg2);

        $this->assertSame($arg1 + $arg2 + ($arg2 - $arg1), $result);
    }
}