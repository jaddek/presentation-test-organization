<?php

declare(strict_types=1);

namespace App\Tests\Example\Example1;

use App\Coefficient;
use App\Exception\ArgumentException;
use App\Exception\EqualException;
use Faker\Factory;
use PHPUnit\Framework\TestCase;

/**
 * Class CoefficientTest
 * @package App\Tests\Example\Example1
 */
class CoefficientTest extends TestCase
{
    /**
     * @return array
     */
    public function calculateSuccessProvider()
    {
        $faker = Factory::create();

        return [
            [
                $faker->numberBetween(0, 10),
                $faker->numberBetween(),
            ],
        ];
    }

    /**
     * @dataProvider calculateSuccessProvider
     *
     * @param $arg1
     * @param $arg2
     * @throws \App\Exception\ArgumentException
     * @throws \App\Exception\EqualException
     */
    public function testCalculateSuccess($arg1, $arg2)
    {
        $coefficient     = new Coefficient();
        $calculateResult = $coefficient->calculate($arg1, $arg2);

        $this->assertSame($arg2 - $arg1, $calculateResult);
    }

    /**
     * @return array
     */
    public function calculateExpectArgumentExceptionProvider()
    {
        $faker = Factory::create();

        return [
            [
                $faker->numberBetween(10),
                $faker->numberBetween(),
            ],
        ];
    }


    /**
     * @dataProvider calculateExpectArgumentExceptionProvider
     *
     * @param $arg1
     * @param $arg2
     * @throws \App\Exception\ArgumentException
     * @throws \App\Exception\EqualException
     */
    public function testCalculateExpectArgumentException($arg1, $arg2)
    {
        $this->expectException(ArgumentException::class);
        $this->expectExceptionMessage('Argument 1 too low');

        $coefficient     = new Coefficient();
        $coefficient->calculate($arg1, $arg2);
    }

    /**
     * @return array
     */
    public function calculateEqualExceptionProvider()
    {

        $faker = Factory::create();

        $number = $faker->numberBetween(11);

        return [
            [
                $number,
                $number,
            ],

        ];
    }


    /**
     * @dataProvider calculateEqualExceptionProvider
     *
     * @param $arg1
     * @param $arg2
     * @param $result
     * @throws \App\Exception\ArgumentException
     * @throws \App\Exception\EqualException
     */
    public function testCalculateExpectEqualException($arg1, $arg2)
    {
        $this->expectException(EqualException::class);
        $this->expectExceptionMessage('Arguments cant be equal');

        $coefficient     = new Coefficient();
        $coefficient->calculate($arg1, $arg2);
    }
}