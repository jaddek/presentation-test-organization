<?php

declare(strict_types=1);

namespace App\Tests;

use App\Calculator;
use App\Coefficient;
use Faker\Factory;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{
    /**
     * @throws \App\Exception\ArgumentException
     */
    public function testSumBadExample()
    {
        /** @var Coefficient $coefficientMock */
        $coefficientMock = $this->createMock(Coefficient::class);

        $calculator = new Calculator($coefficientMock);
        $result     = $calculator->sum(1, 10);

        $this->assertSame(11, $result);
    }

    /**
     * @return array
     */
    public function sumProvider()
    {
        $faker = Factory::create();

        return [
            [
                $faker->numberBetween(),
                $faker->numberBetween(),
            ],
            [
                $faker->numberBetween(),
                $faker->numberBetween(),
            ],
        ];
    }

    /**
     * @dataProvider sumProvider
     *
     * @param $arg1
     * @param $arg2
     * @throws \App\Exception\ArgumentException
     */
    public function testSumGoodExample($arg1, $arg2)
    {
        /** @var Coefficient $coefficientMock */
        $coefficientMock = $this->createMock(Coefficient::class);

        $calculator = new Calculator($coefficientMock);
        $result     = $calculator->sum($arg1, $arg2);

        $this->assertSame($arg1 + $arg2, $result);
    }

    /**
     * @return array
     */
    public function squareOfSumProvider()
    {
        $faker = Factory::create();

        return [
            [
                $faker->numberBetween(),
                $faker->numberBetween(),
            ],
            [
                $faker->numberBetween(),
                $faker->numberBetween(),
            ],
        ];
    }

    /**
     * @dataProvider squareOfSumProvider
     *
     * @param $arg1
     * @param $arg2
     * @throws \App\Exception\ArgumentException
     */
    public function testSquareOfSum($arg1, $arg2)
    {
        /** @var Coefficient $coefficientMock */
        $coefficientMock = $this->createMock(Coefficient::class);

        $calculator = new Calculator($coefficientMock);
        $result     = $calculator->squareOfSum($arg1, $arg2);

        $this->assertSame((($arg1 + $arg2) * ($arg1 + $arg2)), $result);
    }


    /**
     * @return array
     */
    public function sumPlusCoefficientProvider()
    {
        $faker = Factory::create();

        return [
            [
                $faker->numberBetween(),
                $faker->numberBetween(),
            ],
            [
                $faker->numberBetween(),
                $faker->numberBetween(),
            ],
        ];
    }

    /**
     * @dataProvider sumPlusCoefficientProvider
     *
     * @param $arg1
     * @param $arg2
     */
    public function testSumPlusCoefficient($arg1, $arg2)
    {
        $coefficient     = 1;
        $coefficientMock = $this->createMock(Coefficient::class);
        $coefficientMock->method('calculate')->willReturn($coefficient);


        $calculator = new Calculator($coefficientMock);
        $result     = $calculator->sumPlusCoefficient($arg1, $arg2);

        $this->assertSame($arg1 + $arg2 + $coefficient, $result);
    }
}